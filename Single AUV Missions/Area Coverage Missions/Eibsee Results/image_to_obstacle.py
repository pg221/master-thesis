import numpy as np
from PIL import Image
import matplotlib.pyplot as mp

BLOCK_SIZE = 1

im = Image.open('dv.bmp')
pixels = im.load()
width, height = im.size
offset = int(BLOCK_SIZE / 2)

maze = [
  [
    int(pixels[x + offset, y + offset][0] < 125)
    for y in range(0, height, BLOCK_SIZE)
  ]
  for x in range(0, width, BLOCK_SIZE)
]
print(maze[0])
arr = np.array(maze)
arr[65][90] = 2
np.save("obstacles_data.npy",arr)
#mp.imsave("Image_from_array.png", arr)
