"""
Env 2D
@author: huiming zhou
"""
import random
import numpy as np
from PIL import Image
#num1 = random.randint(5, 45)
#num2 = random.randint(5, 25)

BLOCK_SIZE = 2

im = Image.open('dv.bmp')
pixels = im.load()
width, height = im.size
offset = int(BLOCK_SIZE / 2)

maze = [
  [
    int(pixels[x + offset, y + offset][0] < 125)
    for y in range(0, height, BLOCK_SIZE)
  ]
  for x in range(0, width, BLOCK_SIZE)
]
"""
for i, val in enumerate(maze):
    for j in range(len(maze[0])):
        if maze[i][j] == 1:
480 372            print(maze[i][j])
"""
start = (50,40)#50 40
goal = (90,30)

print(len(maze[0]))
print(len(maze))
class Env:

    def __init__(self):
        self.x_range = len(maze[0])  # size of background
        self.y_range = len(maze)
        self.motions = [(-1, 0), (-1, 1), (0, 1), (1, 1),
                        (1, 0), (1, -1), (0, -1), (-1, -1)]
        self.obs = self.obs_map()

    def update_obs(self, obs):
        self.obs = obs

    def obs_map(self):
        """
        Initialize obstacles' positions
        :return: map of obstacles
        """

        x = self.x_range
        y = self.y_range
        obs = set()

        """
        for i in range(x):
            obs.add((i, 0))
        for i in range(x):
            obs.add((i, y - 1))

        for i in range(y):
            obs.add((0, i))
        for i in range(y):
            obs.add((x - 1, i))
        """
        #for i, val in enumerate(maze):
        for i, val in enumerate(maze):
            for j in range(len(maze[0])):
                if maze[i][j] == 1:
                    obs.add((i, j))
        #obs.add((num1 , num2))

        """
        for i in range(10, 21):
            obs.add((i, 15))
        for i in range(15):
            obs.add((20, i))

        for i in range(15, 30):
            obs.add((30, i))
        for i in range(16):
            obs.add((40, i))
        """
        return obs
if (start == first node and goal == last node)
    {
        path successfuly found
    }
else
    {
        path not found
    }
