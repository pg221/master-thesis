import numpy as np

path = np.load("BestFirst_path.npy")

with open('path.txt','w') as f:
    for i,xy in enumerate(path):
        f.write('{} {} {} '.format(i,xy[0],xy[1]))
