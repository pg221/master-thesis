import math
import numpy as np
from scipy.spatial import distance
import csv
import os
import sys
import matplotlib.pyplot as plt
import pandas as pd
from IPython.display import display


sys.path.append(os.path.dirname(os.path.abspath(__file__)) +
                "/../../Search_based_Planning/")

from Search_2D import plotting, env



#os.system('python Anytime_D_star.py')
#os.system('python ARAstar.py')
os.system('python Astar.py')
os.system('python Best_First.py')
#os.system('python bfs.py')
os.system('python Bidirectional_a_star.py')
os.system('python D_star_Lite.py')
os.system('python D_star.py')
#os.system('python dfs.py')
#os.system('python Dijkstra.py')
#os.system('python LPAstar.py')
#os.system('python LRTAstar.py')
#os.system('python RTAAstar.py')

def distance2(algorithm):
    len1 = len(algorithm)-1
    length = 0.00
    for i in range(len1):
        length+=distance.euclidean(algorithm[i],algorithm[i+1])
    #print(length)
    return(length)

Anytime_D_star = np.load("Anytime_D_star_path.npy")
ARAstar = np.load("ARAstar_path.npy")
Astar = np.load("Astar_path.npy")
Best_First = np.load("BestFirst_path.npy")
bfs = np.load("bfs_path.npy")
Bidirectional_a_star=np.load("Bidirectional_a_star_path.npy")
D_star_Lite = np.load("D_star_Lite_path.npy")
D_star = np.load("D_star_path.npy")
dfs = np.load("dfs_path.npy")
Dijkstra = np.load("dijkstra_path.npy")
LPAstar = np.load("LPAstar_path.npy")
LRTAstar = np.load("LRTA_path.npy")
RTAAstar = np.load("RTAAstar_path.npy")
algo=[Anytime_D_star,ARAstar,Astar,Best_First,bfs,Bidirectional_a_star,D_star_Lite,D_star,dfs,Dijkstra,LPAstar,LRTAstar,RTAAstar]

goal =np.load("Goal.npy")

#1. calculating computational time
Anytime_D_star_time = np.load("Anytime_D_star_time.npy")
ARAstar_time = np.load("ARAstar_time.npy")
Astar_time = np.load("Astar_time.npy")
BestFirst_time = np.load("BestFirst_time.npy")
bfs_time = np.load("bfs_time.npy")
Bidirectional_a_star_time = np.load("Bidirectional_a_star_time.npy")
D_star_Lite_time = np.load("D_star_Lite_time.npy")
D_star_time = np.load("D_star_time.npy")
dfs_time = np.load("dfs_time.npy")
Dijkstra_time = np.load("Dijkstra_time.npy")
LPAstar_time = np.load("LPAstar_time.npy")
LRTAstar_time = np.load("LRTAstar_time.npy")
RTAAstar_time = np.load("RTAAstar_time.npy")


algo_time=[Anytime_D_star_time,ARAstar_time,Astar_time,BestFirst_time,bfs_time,Bidirectional_a_star_time,D_star_Lite_time,D_star_time,dfs_time,Dijkstra_time,LPAstar_time,LRTAstar_time,RTAAstar_time]

computational_time = []
for i in algo_time:
    computational_time.append(i)
x= np.array(computational_time)
plt.plot(computational_time)
plt.title('Computational time')
plt.xlabel('Algorithms',fontdict ={'fontsize':10})
plt.ylabel('Computational_time',fontdict ={'fontsize':10})
plt.savefig("plots/{}.png".format("computational_time"))
#plt.show()

#2. calculating success rate
sucess = []
goal = env.goal
counter = 0
for i in algo:
    sucess.insert(counter,0)
    for j in i:
        if (goal == j).all():
            sucess[counter] = 1
counter+=1



#3. distance left to goal
distance_to_goal = []
#distance.euclidean(x,y)
counter = 0
for i in algo:
    #if (i[-1] == goal).all():
    distance_to_goal.insert(counter,0)
    #else:
    distance_to_goal[counter] = distance.euclidean(i[0],goal)
    for j in i:
        if (distance.euclidean(j,goal) < distance_to_goal[counter]).all():
            distance_to_goal[counter] = distance.euclidean(j,goal)
counter+=1


"""""
print(Anytime_D_star.shape)
print(ARAstar.shape)
print(Astar.shape)
print(Best_First.shape)
print(bfs.shape)
print(Bidirectional_a_star.shape)
print(D_star_Lite.shape)
print(D_star.shape)
print(dfs.shape)
print(Dijkstra.shape)
print(LPAstar.shape)
print(LRTAstar.shape)
print(RTAAstar.shape)

"""""
plt.clf()
#4. calculating path length
distances = []
for i in algo:
    distances.append(distance2(i))
x= np.array(distances)
plt.plot(distances)
plt.title('Path length')
plt.xlabel('Algorithms',fontdict ={'fontsize':10})
plt.ylabel('Path_length',fontdict ={'fontsize':10})
plt.savefig("plots/path.png")
#plt.show()

#5. Path deviation
path_deviation = (x/min(x)-1)*100


algo_name=["Anytime_D_star","ARAstar","Astar","Best_First","bfs","Bidirectional_a_star","D_star_Lite","D_star","dfs","Dijkstra","LPAstar","LRTAstar","RTAAstar"]
dataset = pd.DataFrame({'algo_name':algo_name,'computational_time': algo_time, 'Algorithms': distances,'sucess_rate': sucess,'distance_to_goal':distance_to_goal,'path_deviation':path_deviation}, columns=['algo_name','computational_time', 'Algorithms','sucess_rate','distance_to_goal','path_deviation'])
#display(dataset)



dataset.to_excel (r'D:\thesid\path planninh\Search_based_Planning\Search_2D\excel sheets\export_dataframe.xlsx', index = False, header=True)
