"""
Env 2D
@author: huiming zhou
"""
import random
num1 = random.randint(5, 45)
num2 = random.randint(5, 25)


start = (5, 5)
goal = (45, 5)

class Env:

    def __init__(self):
        self.x_range = 51  # size of background
        self.y_range = 31
        self.motions = [(-1, 0), (-1, 1), (0, 1), (1, 1),
                        (1, 0), (1, -1), (0, -1), (-1, -1)]
        self.obs = self.obs_map()

    def update_obs(self, obs):
        self.obs = obs

    def obs_map(self):
        """
        Initialize obstacles' positions
        :return: map of obstacles
        """

        x = self.x_range
        y = self.y_range
        obs = set()

        for i in range(x):
            obs.add((i, 0))
        for i in range(x):
            obs.add((i, y - 1))

        for i in range(y):
            obs.add((0, i))
        for i in range(y):
            obs.add((x - 1, i))

        #obs.add((num1 , num2))
        for i in range(16,31):
            obs.add((25,i))
        for i in range(0,15):
            obs.add((25,i))
        return obs
