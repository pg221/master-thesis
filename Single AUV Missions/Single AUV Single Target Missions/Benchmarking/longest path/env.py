"""
Env 2D
@author: huiming zhou
"""
import random
num1 = random.randint(5, 45)
num2 = random.randint(5, 25)


start = (3, 28)
goal = (8, 28)

class Env:

    def __init__(self):
        self.x_range = 51  # size of background
        self.y_range = 31
        self.motions = [(-1, 0), (-1, 1), (0, 1), (1, 1),
                        (1, 0), (1, -1), (0, -1), (-1, -1)]
        self.obs = self.obs_map()

    def update_obs(self, obs):
        self.obs = obs

    def obs_map(self):
        """
        Initialize obstacles' positions
        :return: map of obstacles
        """

        x = self.x_range
        y = self.y_range
        obs = set()

        for i in range(x):
            obs.add((i, 0))
        for i in range(x):
            obs.add((i, y - 1))

        for i in range(y):
            obs.add((0, i))
        for i in range(y):
            obs.add((x - 1, i))

        for i in range(5,y):
            obs.add((5,i))
        for i in range(5,46):
            obs.add((i, 5))
        for i in range(5,25):
            obs.add((45, i))
        for i in range(5,46):
            obs.add((i, 25))
        return obs

