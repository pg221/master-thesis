import numpy as np




from PIL import Image

BLOCK_SIZE = 1

im = Image.open('dv.bmp')
pixels = im.load()
width, height = im.size
offset = int(BLOCK_SIZE / 2)

maze = [
  [
    int(pixels[x + offset, y + offset][0] < 125)
    for y in range(0, height, BLOCK_SIZE)
  ]
  for x in range(0, width, BLOCK_SIZE)
]
f = open("demofile2.txt", "a")

for i, val in enumerate(maze):
    for j in range(len(maze[0])):
        if maze[i][j] == 1:
            f.write("- !!python/tuple [{}, {}]\n".format(i,j))

f.close()
